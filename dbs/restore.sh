#!/bin/bash

THEDBUSER="root"
THEDBPW="123qwe"

BASES=(common_cms common_cms_data designlaw dhl dogsi ecms escrow ingenia pruebas)
for BASE_X in ${BASES[*]}
do
	echo "create database ${BASE_X}" | mysql --default-character-set=utf8 -u"${THEDBUSER}" -p"${THEDBPW}"
	gunzip < data/dbbackup_"${BASE_X}"_2016*.sql.gz | mysql --default-character-set=utf8 -u"${THEDBUSER}" -p"${THEDBPW}" "${BASE_X}"
done

BASES=(hotelpaq hotelpaq_backend hotelpaq_logs hotelpaq_scraping)
for BASE_X in ${BASES[*]}
do
	echo "create database ${BASE_X}" | mysql --default-character-set=utf8 -u"${THEDBUSER}" -p"${THEDBPW}"
	mysql --default-character-set=utf8 -uroot -p123qwe "${BASE_X}" < "backup_201603311500/${BASE_X}.sql"
done