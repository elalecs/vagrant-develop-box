#!/bin/bash

THEDBUSER="root"
THEDBPW="123qwe"

create_backup()
{
    THEDATE=`date +%Y%m%d%H%M`

    mysqldump \
        --opt \
        --skip-comments \
        --routines \
        --skip-triggers \
        --skip-lock-tables \
        -u $THEDBUSER \
        -p${THEDBPW} \
        $1 \
        | sed -e 's/DEFINER=`root`@`localhost` //' \
        | gzip > data/dbbackup_${1}_${THEDATE}.sql.gz
}

create_backup "common-cms"
create_backup "common-cms-data"
create_backup "designlaw"
create_backup "dhl"
create_backup "dogsi"
create_backup "ecms"
create_backup "escrow"
create_backup "ingenia"
create_backup "pruebas"
create_backup "stayin"
create_backup "stayin_backend"
create_backup "stayin_logs"
create_backup "stayin_scraping"