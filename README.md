El motivo de tener de esta *box* es compartir un mismo entorno entre las personas
que desarrollen los proyectos y tener un entorno lo más cercano a lo que tenemos
en los servidores.

Para correr esta caja es tener instalado *vagrant* y *dnsmasq*

Las instrucciones para instalar *Vagrant* se encuentran en https://www.vagrantup.com/docs/installation/

Para instalar *DNSMasq* se puede utilzar *brew* usando la siguiente receta:

```bash
brew install dnsmasq
mkdir -pv $(brew --prefix)/etc/
echo 'address=/.dev/127.0.0.1' > $(brew --prefix)/etc/dnsmasq.conf
sudo cp -v $(brew --prefix dnsmasq)/homebrew.mxcl.dnsmasq.plist /Library/LaunchDaemons
sudo launchctl load -w /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
sudo mkdir -v /etc/resolver
sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/dev'
```

Esta caja tiene configurado lo siguiente:

  + Apache
  + Nginx
  + PHP+FPM
  + Forward ports

Tanto *Apache* como *Nginx* apuntan a la carpeta ~/Sites/ del *host*

*Apache* escucha en el puerto *8888*, *Nginx* en el puerto *8080* tanto en el
*guest* como en el *host*

Las carpetas en *~/Sites/* se acceden por dominio con la extensión *dev*, para
abrir la carpeta *~/Sites/mi-proyecto/* a través de *Nginx* la url es
*http://mi-proyecto.dev:8080*

*MariaDB* está configurado para correr sobre el puerto *3306* y en la caja viene
con el usuario *root* usando la contraseña *123qwe*

La arquitectura de archivos y carpetas es:

  + ~/Sites/ - poniendo en carpetas los sitios que se accederan por subdominio
  + ~/Vagrant/ - todos los archivos y configuración de la VM con Vagrant
  + ~/Vagrant/Vagranfile - abajo se anexa su contenido
  + ~/Vagrant/etc/
    + php5/ - config de php y el fpm, abajo se anexan
    + nginx/ - config de nginx y los vhosts, abajo se anexan

La receta para la creación de la *box* de *Vagrant*:

```bash
# setup de directorios
mkdir -p ~/Vagrant/{etc/{php5,nginx,apache},logs/{nginx,php5,apache}}

cd ~/Vagrant/

# archivo config inicial
cat > ~/Vagrant/Vagrantfile <<EOF
Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/trusty64"
  config.vm.hostname = "benderinux"
  config.vm.network "forwarded_port", guest: 8080, host: 8080
  config.vm.network "forwarded_port", guest: 8888, host: 8888
  config.vm.network "forwarded_port", guest: 3306, host: 3306
  config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.provision :shell, path: "iniciarServicios.sh", run: "always", privileged: false
  config.vm.synced_folder "~/Sites", "/var/www/vhosts", owner: "www-data", group: "www-data"
  config.vm.synced_folder "~/Vagrant/etc/nginx", "/etc/nginx/myconfig", owner: "root", group: "root"
  config.vm.synced_folder "~/Vagrant/etc/php5", "/etc/php5/myconfig", owner: "root", group: "root"
end
EOF

cat > ~/Vagrant/iniciarServicios.sh <<EOF
#!/bin/bash
sudo service php5-fpm restart
sudo service mysql restart
sudo service apache2 restart
sudo service nginx restart
EOF

# creamos la VM
vagrant up

# entramos a la VM por SSH
vagrant ssh

# conf de timezone
sudo dpkg-reconfigure tzdata

# instalamos las herramientas adicionales de la VM
sudo apt-get -f install linux-headers-$(uname -r) build-essential dkms
sudo mkdir /media/VBoxGuestAdditions
wget http://download.virtualbox.org/virtualbox/5.0.16/VBoxGuestAdditions_5.0.16.iso
sudo mount -o loop,ro VBoxGuestAdditions_5.0.16.iso /media/VBoxGuestAdditions
sudo bash /media/VBoxGuestAdditions/VBoxLinuxAdditions.run
exit
vagrant reload
vagrant ssh
rm VBoxGuestAdditions_5.0.16.iso

# LAMP
sudo apt-get -f install mariadb-server mariadb-client nginx apache2 php5 libapache2-mod-php5 php5-cli php5-curl php5-fpm php5-imagick php5-mcrypt php5-mysqlnd php5-xcache php5-xdebug mc

sudo a2enmod vhost_alias

sudo sed -i.bak "s/Listen 80/Listen 8888/g" /etc/apache2/ports.conf

sudo sed -i.bak "s/short_open_tag = Off/short_open_tag = On/g" /etc/php5/fpm/php.ini
sudo sed -i.bak "s/error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/error_reporting = E_ALL/g" /etc/php5/fpm/php.ini
sudo sed -i.bak "s/display_errors = Off/display_errors = On/g" /etc/php5/fpm/php.ini
sudo sed -i.bak "s/display_startup_errors = Off/display_startup_errors = On/g" /etc/php5/fpm/php.ini
sudo sed -i.bak "s/html_errors = On/html_errors = Off/g" /etc/php5/fpm/php.ini

sudo sed -i.bak "s/short_open_tag = Off/short_open_tag = On/g" /etc/php5/apache2/php.ini
sudo sed -i.bak "s/error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/error_reporting = E_ALL/g" /etc/php5/apache2/php.ini
sudo sed -i.bak "s/display_errors = Off/display_errors = On/g" /etc/php5/apache2/php.ini
sudo sed -i.bak "s/display_startup_errors = Off/display_startup_errors = On/g" /etc/php5/apache2/php.ini
sudo sed -i.bak "s/html_errors = On/html_errors = Off/g" /etc/php5/apache2/php.ini

sudo sed -i.bak "s/fastcgi_param.*SERVER_NAME.*\$server_name;/fastcgi_param    SERVER_NAME    \$host;/g" /etc/nginx/fastcgi_params

# Setup MariaDB para permitir acceso remoto
sudo sed -i.bak "s/\= 127\.0\.0\.1/= 0.0.0.0/g" /etc/mysql/my.cnf
echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123qwe' WITH GRANT OPTION; FLUSH PRIVILEGES;" | mysql -uroot -p

cat > /tmp/vhosts.conf <<EOF
#
# Use name-based virtual hosting.
#
# NameVirtualHost *:8888
ServerName localhost

LogFormat "%V %h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combinedmassvhost

# Auto-VirtualHosts with .dev
<VirtualHost *:8888>
  ServerName dev
  ServerAlias *.dev

  CustomLog "/var/www/vhosts/logs/dev-access_log" combinedmassvhost
  ErrorLog "/var/www/vhosts/logs/dev-error_log"

  VirtualDocumentRoot /var/www/vhosts/%-2+
  <Directory "/var/www/vhosts/*">
    Options FollowSymLinks
    AllowOverride All

    Order allow,deny
    Allow from all
  </Directory>
</VirtualHost>

# Auto-VirtualHosts with lo.midojo.mx
<VirtualHost *:8888>
  ServerName lo.midojo.mx
  ServerAlias *.lo.midojo.mx

  CustomLog "/var/www/vhosts/logs/dev-access_log" combinedmassvhost
  ErrorLog "/var/www/vhosts/logs/dev-error_log"

  VirtualDocumentRoot /var/www/vhosts/%-7+
  <Directory "/var/www/vhosts/*">
    Options FollowSymLinks
    AllowOverride All

    Order allow,deny
    Allow from all
  </Directory>
</VirtualHost>
EOF
sudo mv /tmp/vhosts.conf /etc/apache2/sites-available/000-default.conf

sudo a2enmod rewrite
sudo service apache2 restart

# LEMP
sudo apt-get -f nginx

cat > /tmp/001_hpaq-api.dev <<EOF
server {
  listen 8080;
  server_name hpaq-api.dev hpaq-api.lo.midojo.mx;
  client_max_body_size 1m;

  root /var/www/vhosts/hpaq-api;
  index index.html index.htm index.php;

  access_log /vagrant/logs/nginx/hpaq-api-access.log;
  error_log /vagrant/logs/nginx/hpaq-api-error.log debug;

  location ~ /\.git {
    deny all;
  }

  location / {
    try_files \$uri \$uri/ /index.php?\$is_args\$args;
    autoindex off;
    index index.html index.htm index.php;
    rewrite ^/([a-zA-Z0-9_-]+)\/?+\$ /index.php?resource=\$1 last;
  }

  location ~ \.php\$ {
    fastcgi_index index.php;
    fastcgi_split_path_info ^(.+\.php)(/.*)\$;
    try_files \$uri \$uri/ /index.php\$is_args\$args;
    include /etc/nginx/fastcgi_params;
    #fastcgi_pass 127.0.0.1:9000;
    fastcgi_pass unix:/var/run/php5-fpm.sock;
    fastcgi_param APP_ENV dev;
  }

  sendfile off;
}
EOF
sudo mv /tmp/001_hpaq-api.dev /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/001_hpaq-api.dev /etc/nginx/sites-enabled/

cat > /tmp/002_hpaq-be.dev <<EOF
server {
  listen 8080;
  server_name    ~^(hpaq-be-.*|hpaq-be)\.(dev|lo\.midojo\.mx)\$;
  set \$sub \$1;
  root    /var/www/vhosts/\$sub;
  client_max_body_size 1m;

  index index.html index.htm index.php;

  error_log /vagrant/logs/nginx/vhosts-error.log debug;

  location ~ /\.git {
    deny all;
  }

  location /phrapi {
  }

  location ~ ^/(do|json|run|alone|page|site) {
    rewrite ^/(do|json|run|alone|page|site)=(.*)\$ /index.php?\$1=\$2 last;
  }

  location ~ ^/a(\d+) {
    rewrite ^/a(\d+) /article.php?id=\$1 last;
  }

  location ~ ^/c(\d+) {
    rewrite ^/c(\d+) /category.php?id=\$1 last;
  }

  location ~ ^/(terminos-condiciones|privacidad) {
    rewrite ^/(terminos-condiciones|privacidad) /content.php?alias=\$1 last;
  }

  location ~ ^/sitemap.xml {
    rewrite ^/sitemap.xml /phrapi/index.php?resource=sitemap last;
  }

  location / {
    try_files \$uri \$uri/ =404;
    autoindex off;
  }

  location ~ \.php\$ {
    fastcgi_index index.php;
    fastcgi_split_path_info ^(.+\.php)(/.*)\$;
    try_files \$uri \$uri/ /index.php\$is_args\$args;
    include /etc/nginx/fastcgi_params;
    #fastcgi_pass 127.0.0.1:9000;
    fastcgi_pass unix:/var/run/php5-fpm.sock;
    fastcgi_param APP_ENV dev;
  }

  sendfile off;
}
EOF
sudo mv /tmp/002_hpaq-be.dev /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/002_hpaq-be.dev /etc/nginx/sites-enabled/

cat > /tmp/999_all.dev <<EOF
server {
  listen 8080;
  server_name    ~^(.*)\.(dev|lo\.midojo\.mx)\$;
  set \$sub \$1;
  root    /var/www/vhosts/\$sub;
  client_max_body_size 1m;

  index index.html index.htm index.php;

  error_log /vagrant/logs/nginx/vhosts-error.log debug;

  location ~ /\.git {
    deny all;
  }

  location /phrapi {
  }

  location ~ ^/(do|json|run|alone|page|site) {
    rewrite ^/(do|json|run|alone|page|site)=(.*)\$ /index.php?\$1=\$2 last;
  }

  location / {
    try_files \$uri \$uri/ /index.php?\$is_args\$args;
  }

  location ~ \.php\$ {
    fastcgi_index index.php;
    fastcgi_split_path_info ^(.+\.php)(/.*)\$;
    try_files \$uri \$uri/ /index.php\$is_args\$args;
    include /etc/nginx/fastcgi_params;
    #fastcgi_pass 127.0.0.1:9000;
    fastcgi_pass unix:/var/run/php5-fpm.sock;
    fastcgi_param APP_ENV dev;
  }

  sendfile off;
}
EOF
sudo mv /tmp/999_all.dev /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/999_all.dev /etc/nginx/sites-enabled/

sudo service apache2 restart
sudo service php5-fpm restart
sudo service nginx restart

```
